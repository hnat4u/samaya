#include "xmlForm.h"
#include "dataStore.h"

xmlForm::xmlForm(DataStore *head)
{
    dbs = head;
    tableFileName = TASK_LIST_TABLE;
    tableFileName.append(DATA_STORE_TAG);
}

void xmlForm::loadDataFromXml(DataMachine *dm)
{
    QString taskName, taskPriority, taskStatus,
            taskStartTime, taskEndTime, taskActualEndTime;
    QXmlStreamReader Rxml;
    uint32_t idx = 0;
    QFile frd(tableFileName);
    if(!frd.open(QIODevice::ReadOnly))
    {
        return;
    }
    Rxml.setDevice(&frd);
    Rxml.readNext();

    while(!Rxml.atEnd())
    {
        if(Rxml.isStartElement())
        {
            QString task_no("task_");
            task_no.append(QString::number(idx));
            if(Rxml.name() == task_no)
            {
                while(!Rxml.atEnd())
                {
                    if(Rxml.isStartElement())
                    {
                        if(Rxml.name() == SAMAY_TASK_NAME) {
                            taskName = Rxml.readElementText();
                            qDebug()<<Rxml.name()<<taskName;
                        } else if(Rxml.name() == SAMAY_TASK_PRIORITY) {
                            taskPriority = Rxml.readElementText();
                            qDebug()<<Rxml.name()<<taskPriority;
                        } else if(Rxml.name() == SAMAY_TASK_STATUS) {
                            taskStatus = Rxml.readElementText();
                            qDebug()<<Rxml.name()<<taskStatus;
                        } else if(Rxml.name() == SAMAY_TASK_START_DATE) {
                            taskStartTime = Rxml.readElementText();
                            qDebug()<<Rxml.name()<<taskStartTime;
                        } else if(Rxml.name() == SAMAY_TASK_END_DATE) {
                            taskEndTime = Rxml.readElementText();
                            qDebug()<<Rxml.name()<<taskEndTime;
                        } else if(Rxml.name() == SAMAY_TASK_ACT_END_DATE) {
                            taskActualEndTime = Rxml.readElementText();
                            qDebug()<<Rxml.name()<<taskActualEndTime;
                        } else if(Rxml.name() == SAMAY_TASK_UID) {
                            DataStore dataAdd(taskName, taskPriority, taskStatus,
                                              QDateTime::fromString(taskStartTime,SAMAY_DATE_FORMAT),
                                              QDateTime::fromString(taskEndTime,SAMAY_DATE_FORMAT),
                                              QDateTime::fromString(taskActualEndTime, SAMAY_DATE_FORMAT),
                                              "Need to add", Rxml.readElementText().toInt());
                            dm->addDataInDS(dataAdd);
                        }
                    }
                    else if(Rxml.isEndDocument()) {
                        Rxml.readNext();
                        break;
                    }
                    Rxml.readNext();
                }
            }
        }
        Rxml.readNext();
    }
    frd.close();
}

void xmlForm::storeDataAsXml()
{
    DataStore *ll = dbs;
    QFile fwr(tableFileName);
    fwr.open(QIODevice::WriteOnly);
    QXmlStreamWriter xmlWriter(&fwr);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();
    xmlWriter.writeStartElement(TASK_LIST_TABLE);
    for(uint32_t idx=0; ll; idx++) {
        QString task_no("task_");
        task_no.append(QString::number(idx));
        xmlWriter.writeStartElement(task_no);
        xmlWriter.writeTextElement(SAMAY_TASK_NAME, ll->getTaskName());
        xmlWriter.writeTextElement(SAMAY_TASK_PRIORITY, ll->getTaskPriority());
        xmlWriter.writeTextElement(SAMAY_TASK_STATUS, ll->getTaskStatus());
        xmlWriter.writeTextElement(SAMAY_TASK_START_DATE, ll->getStartTime().toString(SAMAY_DATE_FORMAT));
        xmlWriter.writeTextElement(SAMAY_TASK_END_DATE, ll->getEndTime().toString(SAMAY_DATE_FORMAT));
        xmlWriter.writeTextElement(SAMAY_TASK_ACT_END_DATE, ll->getActualEndTime().toString(SAMAY_DATE_FORMAT));
        QString uniqId;
        uniqId.setNum(ll->taskUniqId, 10);
        xmlWriter.writeTextElement(SAMAY_TASK_UID, uniqId);
        xmlWriter.writeEndElement();
        ll = ll->dataLink;
    }
    xmlWriter.writeEndElement();
    fwr.close();
}

xmlForm::~xmlForm()
{

}
