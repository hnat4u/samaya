#ifndef DATASTORE_H
#define DATASTORE_H
#include <QString>
#include <QDateTime>
#include "dataCommon.h"

class DataStore
{
    QString dataTaskName;
    QString dataTaskStatus;
    QString dataTaskPriority;
    QDateTime dataStartTime;
    QDateTime dataEndTime;
    QDateTime dataActualEndTime;
    QString dataTaskDescription;
public:
    uint32_t taskUniqId;
    DataStore *dataLink;
    DataStore(DataStore *input);
    DataStore(QString tName, QString tPriority, QString tStatus,
                        QDateTime tStartTime, QDateTime tEndTime,
                        QDateTime tActualEndTime, QString dataTaskDescription, uint32_t);
    void copy(DataStore *source);
    QString getTaskName();
    QString getTaskStatus();
    QString getTaskPriority();
    QString getTaskDescription();
    QDateTime getStartTime();
    QDateTime getEndTime();
    QDateTime getActualEndTime();
    void resetTaskDescription();
    void setTaskDescription(QString tDesc);

    ~DataStore();
};

#endif // DATASTORE_H
