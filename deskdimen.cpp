#include "deskDimen.h"

NDesktopDimension::NDesktopDimension()
{
    myDesk=new QDesktopWidget ;//QApplication::desktop();
}
QRect NDesktopDimension::desktop()
{
        //qDebug()<<myDesk->widthMM()*PIX<<"  "<<PIX*myDesk->heightMM()<<"  ";
    return myDesk->screenGeometry(QApplication::desktop());
}
NDesktopDimension::~NDesktopDimension()
{
    delete myDesk;
}
