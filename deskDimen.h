#ifndef _DESKDIMEN_H_
#define _DESKDIMEN_H_
#include<QApplication>
#include<QDesktopWidget>
#include<QRect>
class NDesktopDimension
{
    QDesktopWidget *myDesk;
    public:
    NDesktopDimension();
    QRect desktop();
    ~NDesktopDimension();
};

#endif
