#include "dataStore.h"

DataStore::DataStore(QString tName, QString tPriority, QString tStatus,
                    QDateTime tStartTime, QDateTime tEndTime,
                    QDateTime tActualEndTime, QString tDescription, uint32_t tUid)
{
    dataTaskName = tName;
    dataTaskPriority = tPriority;
    dataTaskStatus = tStatus;
    dataStartTime = tStartTime;
    dataEndTime = tEndTime;
    dataActualEndTime = tActualEndTime;
    dataTaskDescription = tDescription;
    taskUniqId = tUid;
    dataLink = NULL;
    qDebug()<<dataTaskName<<dataTaskPriority;
}
DataStore::DataStore(DataStore *input)
{
    dataTaskName = input->dataTaskName;
    dataTaskPriority = input->dataTaskPriority;
    dataTaskStatus = input->dataTaskStatus;
    dataStartTime = input->dataStartTime;
    dataEndTime = input->dataEndTime;
    dataActualEndTime = input->dataActualEndTime;
    dataTaskDescription = input->dataTaskDescription;
    taskUniqId = input->taskUniqId;
    dataLink = NULL;
}
void DataStore::resetTaskDescription()
{
    dataTaskDescription = "empty";
}

void DataStore::setTaskDescription(QString tDesc)
{
    dataTaskDescription = tDesc;
}

void DataStore::copy(DataStore *source)
{
    this->dataTaskName = source->getTaskName();
    this->dataTaskPriority = source->getTaskPriority();
    this->dataTaskStatus = source->getTaskStatus();
    this->dataStartTime = source->getStartTime();
    this->dataEndTime = source->getEndTime();
    this->dataActualEndTime = source->getActualEndTime();
    this->dataTaskDescription = source->getTaskDescription();
}

QString DataStore::getTaskName()
{
    return dataTaskName;
}

QString DataStore::getTaskStatus()
{
    return dataTaskStatus;
}

QString DataStore::getTaskPriority()
{
    return dataTaskPriority;
}

QString DataStore::getTaskDescription()
{
    return dataTaskDescription;
}

QDateTime DataStore::getStartTime()
{
    return dataStartTime;
}

QDateTime DataStore::getEndTime()
{
    return dataEndTime;
}

QDateTime DataStore::getActualEndTime()
{
    return dataActualEndTime;
}

DataStore::~DataStore()
{

}
