#include "dataMachine.h"
#include "xmlForm.h"

DataMachine::DataMachine()
{
    samaya = NULL;
    uid = DATA_UNIQ_IDS;
    taskCount = 0;
    crypto.setKey(0x0c2ad4a4acb9f023);
    crypto.setCompressionMode(SimpleCrypt::CompressionAlways);
    crypto.setIntegrityProtectionMode(SimpleCrypt::ProtectionHash);
    loadData();
}

bool DataMachine::loadData()
{
    xmlForm form(samaya);
    form.loadDataFromXml(this);
}

bool DataMachine::storeData()
{
    xmlForm form(samaya);
    form.storeDataAsXml();
}

QString DataMachine::loadDescription(uint32_t uniqID)
{
    QString fileName(SAMAY_TASK_DESCRIPTION_FILE);
    QString taskId;
    taskId.setNum(uniqID, 10);
    fileName.append(taskId);
    fileName.append(SAMAY_TASK_DESCRIPTION_FILE_EXTENTION);

    qDebug()<<fileName;
    QFile fdesc(fileName);
    fdesc.open(QFile::ReadOnly);
    QByteArray qb = fdesc.readAll();
#if 0
    QString taskDesc(qb);
#else
    QString taskDesc(crypto.decryptToByteArray(qb));
#endif
    fdesc.close();
    return taskDesc;
}

bool DataMachine::saveDescription(DataStore dataToStore)
{
    QString fileName(SAMAY_TASK_DESCRIPTION_FILE);
    QString taskId;
    taskId.setNum(dataToStore.taskUniqId, 10);
    fileName.append(taskId);
    fileName.append(SAMAY_TASK_DESCRIPTION_FILE_EXTENTION);

    QFile fdesc(fileName);
    fdesc.open(QFile::WriteOnly);
#if 0
    fdesc.write(dataToStore.getTaskDescription().toUtf8());
#else
    QString descEnc(dataToStore.getTaskDescription().toUtf8());
    //Encryption
    fdesc.write(crypto.encryptToByteArray(descEnc));
#endif
    fdesc.close();
    qDebug()<<fileName;
}

uint32_t DataMachine::addDataInDS(DataStore dataToStore)
{
    DataStore *task, *ll;
    task = new DataStore(&dataToStore);
    if(dataToStore.taskUniqId >= DATA_UNIQ_IDS) {
        qDebug()<<"Already data had UID!!!";
        if(dataToStore.taskUniqId > uid) {
            uid = dataToStore.taskUniqId;
        }
    } else {
        task->taskUniqId = uid;
    }
    qDebug()<<"Enter add data with UID : "<<task->taskUniqId;
    ll = samaya;
    if(samaya == NULL)
    {
        samaya = task;
    }
    else
    {
        while(ll->dataLink) {
            if(ll == NULL)
                break;
            ll = ll->dataLink;
        }
        ll->dataLink = task;
    }
    uid++;
    taskCount++;
    qDebug()<<"Added Task";
    return task->taskUniqId;
}

DataStore* DataMachine::getHeadPin()
{
    return samaya;
}

uint32_t DataMachine::getTaskCount()
{
    return taskCount;
}

bool DataMachine::deleteDataInDS(uint32_t uniqID)
{
    DataStore *ll = samaya, *prev = samaya;
    for(uint32_t idx; ll; idx++) {
        qDebug()<<"pitch one";
        if(uniqID == ll->taskUniqId) {
            qDebug()<<uniqID<<"To delete found";
            if(ll == samaya) {
                samaya = samaya->dataLink;
                delete ll;
                break;
            } else {
                prev->dataLink = ll->dataLink;
                delete ll;
                break;
            }
        }
        prev = ll;
        ll = ll->dataLink;
    }
}

bool DataMachine::editDataInDS(DataStore dataToEdit)
{
    qDebug()<<"Edit mode ?!!!"<<dataToEdit.taskUniqId;
    DataStore *ll = samaya;
    for(uint32_t idx; ll; idx++) {
        if(ll->taskUniqId == dataToEdit.taskUniqId) {
            qDebug()<<"Edit data found";
            ll->copy(&dataToEdit);
            break;
        }
        ll = ll->dataLink;
    }
}

DataMachine::~DataMachine()
{
}
