#ifndef DATACOMMON_H
#define DATACOMMON_H

#include <QDebug>
#define DATA_UNIQ_IDS (0x00001000)
#define SAM_TASK_NAME_SIZE (180)
#define SAM_TASK_STATUS_SIZE (20)
#define SAM_TASK_PRIORITY (20)


#define SAMAY_TASK_COM "Task"
#define SAMAY_SPACE " "
#define SAMAY_TASK_NAME "Task_Name"
#define SAMAY_TASK_PRIORITY "Task_Priority"
#define SAMAY_TASK_STATUS "Task_Status"
#define SAMAY_TASK_START_DATE "Start_Date"
#define SAMAY_TASK_END_DATE "End_Date"
#define SAMAY_TASK_ACT_END_DATE "Actual_End_Date"
#define SAMAY_TASK_UID "UID"

#define SAMAY_DATE_FORMAT "dd-MMMM-yyyy,hh:mm:ss"

#define DATA_STORE_TAG ".dbsnato"
#define TASK_LIST_TABLE "task_list"

#define SAMAY_TASK_DESCRIPTION_FILE "task_description_"
#define SAMAY_TASK_DESCRIPTION_FILE_EXTENTION ".nenc"

enum SAMAYA_MODES {
    SAMAYA_MODE_MAIN = 0,
    SAMAYA_MODE_ADD,
    SAMAYA_MODE_EDIT,
};

enum SAMAYA_TABLE_ITEMS {
    SAM_TaskName = 0,
    SAM_Priority,
    SAM_Status,
    SAM_Start_Date,
    SAM_End_Date,
    SAM_Act_End_Date,
    SAM_TaskUid,
    SAM_TABLE_ITEM_END
};

#endif // DATACOMMON_H
