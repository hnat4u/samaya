#-------------------------------------------------
#
# Project created by QtCreator 2020-06-11T22:34:08
#
#-------------------------------------------------

QT       += core gui
QT += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = samaya
TEMPLATE = app


SOURCES += main.cpp\
        dialog.cpp \
    deskdimen.cpp \
    dataStore.cpp \
    dataMachine.cpp \
    xmlForm.cpp \
    natEncryption.cpp

HEADERS  += dialog.h \
    deskDimen.h \
    dataStore.h \
    dataMachine.h \
    xmlForm.h \
    dataCommon.h \
    natEncryption.h

CONFIG(release , debug|release):DEFINES += QT_NO_DEBUG_OUTPUT
