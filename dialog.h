#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QPushButton>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QTableWidget>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QComboBox>
#include <QDateTimeEdit>
#include <QDateTime>
#include <QDebug>
#include <QMessageBox>
#include "deskDimen.h"
#include "dataMachine.h"

#define WIDTH 800
#define HEIGHT 600


class Dialog : public QDialog
{
    Q_OBJECT
    DataMachine dataMachine;
    enum SAMAYA_MODES my_mode;
    uint32_t taskUniqID;
/*Main part*/
    QGridLayout *mainLayout;
    QHBoxLayout *mainLayoutButtonPanel;
    QPushButton *addTask, *deleteTask, *editTask;
    QTableWidget *taskList;
    int rowSelected;
    void accessMainLayout(bool access);
    QLabel *designerName;
/*Input part*/
    QHBoxLayout *inputLayoutBox[3];
    QGridLayout *inputLayout;
    QLabel *taskNameLabel;
    QLabel *priorityLabel;
    QLabel *statusLabel;
    QLabel *startDateLabel;
    QLabel *endDateLabel;
    QLabel *descriptionLabel;
    QLabel *taskStartTimeLabel;
    QLabel *taskEndTimeLabel;
    QLabel *taskActualEndTimeLabel;
    QLineEdit *taskNameInput;
    QComboBox *priorityInput;
    QComboBox *statusInput;
    QDateTimeEdit *taskStartTime;
    QDateTimeEdit *taskEndTime;
    QDateTimeEdit *taskActualEndTime;
    QTextEdit *taskDescription;
    QPushButton *exitButton;
    QPushButton *saveButton;
    QPushButton *editButton;
    QPushButton *saveAndExitButton;
    void accessInputLayout(bool access);
    void setInputEntry(bool access);

    QGridLayout *outputLayout;
    NDesktopDimension *windowDimen;
    void windowPositioning();
    void updateTable();
public:
    Dialog(QWidget *parent = 0);
    ~Dialog();
public slots:
    void addTaskSignal();
    void deleteTaskSignal();
    void exitTaskSignal();
    void saveTaskSignal();
    void saveAndExitSignal();
    void tableCellSelected(int row, int col);
    void tableCellDoubleClicked(int row, int col);
    void inputEditTaskSignal();
    void editTaskSignal();

    void textChanged(QString dat);
    void descChanged();
    void dateTimeChanged(QDateTime dat);
};

#endif // DIALOG_H
