#ifndef DATAMACHINE_H
#define DATAMACHINE_H
#include <QDebug>
#include <QFile>
#include "dataStore.h"
#include <natEncryption.h>

class DataMachine
{
    DataStore *samaya;
    uint32_t uid;
    uint32_t taskCount;
    SimpleCrypt crypto;
public:
    DataMachine();
    ~DataMachine();
    bool storeData();
    bool loadData();
    uint32_t addDataInDS(DataStore dataToStore);
    bool deleteDataInDS(uint32_t uniqID);
    bool editDataInDS(DataStore dataToEdit);

    bool saveDescription(DataStore dataToStore);
    QString loadDescription(uint32_t uniqID);
    DataStore* getHeadPin();
    uint32_t getTaskCount();
};

#endif // DATAMACHINE_H
