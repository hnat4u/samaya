#ifndef XMLFORM_H
#define XMLFORM_H
#include <QFile>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QDebug>
#include "dataMachine.h"

class xmlForm
{
    DataStore *dbs;
    QString tableFileName;
public:
    xmlForm(DataStore *dbs);
    ~xmlForm();
    void storeDataAsXml();
    void loadDataFromXml(DataMachine *dm);
};

#endif // XMLFORM_H
