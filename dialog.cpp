#include "dialog.h"

void Dialog::windowPositioning()
{
    windowDimen = new NDesktopDimension();
    QRect desktop = windowDimen->desktop();
    setGeometry(desktop.width()/2-WIDTH/2,desktop.height()/2-HEIGHT/2,WIDTH,HEIGHT);
    setFixedSize(WIDTH,HEIGHT);
    //mainLayout->setGeometry(desktop);
    //inputLayout->setGeometry(desktop);
}

void Dialog::accessMainLayout(bool access)
{
    addTask->setVisible(access);
    deleteTask->setVisible(access);
    taskList->setVisible(access);
    designerName->setVisible(access);
    mainLayoutButtonPanel->setEnabled(access);
    //mainLayout->setEnabled(access);
}

void Dialog::setInputEntry(bool access)
{
    taskNameInput->setEnabled(access);
    priorityInput->setEnabled(access);
    statusInput->setEnabled(access);
    taskStartTime->setEnabled(access);
    taskEndTime->setEnabled(access);
    taskActualEndTime->setEnabled(access);
    taskDescription->setEnabled(access);
}

void Dialog::accessInputLayout(bool access)
{
    taskNameInput->setVisible(access);
    taskNameLabel->setVisible(access);
    priorityLabel->setVisible(access);
    priorityInput->setVisible(access);
    statusLabel->setVisible(access);
    statusInput->setVisible(access);
    inputLayout->setEnabled(access);
    taskStartTime->setVisible(access);
    taskStartTimeLabel->setVisible(access);
    taskEndTime->setVisible(access);
    taskEndTimeLabel->setVisible(access);
    taskActualEndTime->setVisible(access);
    taskActualEndTimeLabel->setVisible(access);
    taskDescription->setVisible(access);
    saveButton->setVisible(access);
    saveAndExitButton->setVisible(access);
    editButton->setVisible(access);
    exitButton->setVisible(access);
    setInputEntry(true);
}

void Dialog::updateTable()
{
    DataStore *ds = dataMachine.getHeadPin();
    DataStore *ll = ds;
    for(int idx=0; ll; idx++)
    {
        qDebug()<<ll->getTaskName();
        taskList->setRowCount(idx+1);

        taskList->setItem(idx, SAM_TaskName, new QTableWidgetItem(ll->getTaskName()));
        taskList->setItem(idx, SAM_Status, new QTableWidgetItem(ll->getTaskStatus()));
        taskList->setItem(idx, SAM_Priority, new QTableWidgetItem(ll->getTaskPriority()));
        taskList->setItem(idx, SAM_Start_Date, new QTableWidgetItem(ll->getStartTime().toString(SAMAY_DATE_FORMAT)));
        taskList->setItem(idx, SAM_End_Date, new QTableWidgetItem(ll->getEndTime().toString(SAMAY_DATE_FORMAT)));
        taskList->setItem(idx, SAM_Act_End_Date, new QTableWidgetItem(ll->getActualEndTime().toString(SAMAY_DATE_FORMAT)));
        taskList->setItem(idx, SAM_TaskUid, new QTableWidgetItem(QString::number(ll->taskUniqId, 10)));
        ll = ll->dataLink;
    }
}

void Dialog::addTaskSignal()
{
    accessMainLayout(false);
    accessInputLayout(true);
    editButton->setEnabled(false);
    saveButton->setEnabled(false);
    saveAndExitButton->setEnabled(false);
    my_mode = SAMAYA_MODE_ADD;
    taskNameInput->clear();
    taskDescription->clear();
    priorityInput->setCurrentIndex(0);
    statusInput->setCurrentIndex(0);

    taskStartTime->setDateTime(QDateTime::currentDateTime());
    taskEndTime->setDateTime(QDateTime::currentDateTime());
    taskActualEndTime->setDateTime(QDateTime::currentDateTime());
}

void Dialog::exitTaskSignal()
{
    if(saveButton->isEnabled()){
        switch( QMessageBox::question(
                    this,
                    tr("Exit ?"),
                    tr("Continue without saving..."),

                    QMessageBox::Yes |
                    QMessageBox::Cancel,

                    QMessageBox::Cancel ) )
        {
        case QMessageBox::Yes:
            qDebug( "yes" );
            break;
        case QMessageBox::Cancel:
            qDebug( "cancel" );
        default:
            qDebug( "close" );
            return;
            break;
        }
    }
    accessMainLayout(true);
    accessInputLayout(false);
    my_mode = SAMAYA_MODE_MAIN;
    taskUniqID = 0;
}

void Dialog::inputEditTaskSignal()
{
    editButton->setEnabled(false);
    setInputEntry(true);
}

void Dialog::saveTaskSignal()
{
    DataStore dataStore(taskNameInput->text(), priorityInput->currentText(), statusInput->currentText(),
                        taskStartTime->dateTime(), taskEndTime->dateTime(), taskActualEndTime->dateTime(), taskDescription->toHtml(), 0);

    dataStore.resetTaskDescription();
    if (my_mode == SAMAYA_MODE_ADD) {
        taskUniqID = dataMachine.addDataInDS(dataStore);
        my_mode = SAMAYA_MODE_EDIT;
        dataStore.taskUniqId = taskUniqID;
    } else if (my_mode == SAMAYA_MODE_EDIT) {
        if(taskUniqID != 0) {
            dataStore.taskUniqId = taskUniqID;
        } else {
            qDebug()<<"error : no task id found in edit mode";
            while(1);
        }
        dataMachine.editDataInDS(dataStore);
    } else {
        qDebug()<<"Mode unknown !!!";
        return;
    }
    dataMachine.storeData();
    dataStore.setTaskDescription(taskDescription->toHtml());
    dataMachine.saveDescription(dataStore);
    saveButton->setEnabled(false);
    saveAndExitButton->setEnabled(false);
    updateTable();
}
void Dialog::saveAndExitSignal()
{
    saveTaskSignal();
    exitTaskSignal();
    my_mode = SAMAYA_MODE_MAIN;
    taskUniqID = 0;
}

void Dialog::tableCellSelected(int row, int col)
{
    qDebug()<<row<<col;
    taskList->selectRow(row);
    rowSelected = row;
    editTask->setEnabled(true);
    deleteTask->setEnabled(true);
}

void Dialog::tableCellDoubleClicked(int row, int col)
{
    qDebug()<<row<<col<<"Double clicked";
#if 1
    taskNameInput->setText(taskList->item(row, SAM_TaskName)->text());
    priorityInput->setCurrentText(taskList->item(row, SAM_Priority)->text());
    statusInput->setCurrentText(taskList->item(row, SAM_Status)->text());
    taskStartTime->setDateTime(QDateTime::fromString(taskList->item(row, SAM_Start_Date)->text(), SAMAY_DATE_FORMAT));
    taskEndTime->setDateTime(QDateTime::fromString(taskList->item(row, SAM_End_Date)->text(), SAMAY_DATE_FORMAT));
    taskActualEndTime->setDateTime(QDateTime::fromString(taskList->item(row, SAM_Act_End_Date)->text(), SAMAY_DATE_FORMAT));
    taskUniqID = taskList->item(row, SAM_TaskUid)->text().toInt();
    taskDescription->setHtml(dataMachine.loadDescription(taskUniqID));
    my_mode = SAMAYA_MODE_EDIT;
    accessMainLayout(false);
    accessInputLayout(true);
    editButton->setEnabled(true);
    saveButton->setEnabled(false);
    saveAndExitButton->setEnabled(false);

    setInputEntry(false);
#endif
}

void Dialog::editTaskSignal()
{
    if (rowSelected >= 0) {
        tableCellDoubleClicked(rowSelected, 0);
        rowSelected = -1;
    } else {
        qDebug()<<"no row selected !!!";
    }
}

void Dialog::deleteTaskSignal()
{
    if(rowSelected < 0) {
        qDebug()<<"No row selected";
    } else {
        qDebug()<<"Row selected "<< rowSelected;
        qDebug()<<taskList->item(rowSelected, SAM_TaskUid)->text().toInt();
        switch( QMessageBox::question(
                    this,
                    tr("Delete Task"),
                    tr("Record permanently deleted"),
                    QMessageBox::Cancel |
                    QMessageBox::Yes,
                    QMessageBox::Cancel ))
        {
        case QMessageBox::Yes:
            qDebug( "yes" );
            break;
        case QMessageBox::No:
        default:
            qDebug( "no" );
            return;
            break;
        }

        dataMachine.deleteDataInDS(taskList->item(rowSelected, SAM_TaskUid)->text().toInt());
        taskList->removeRow(rowSelected);
        dataMachine.storeData();
        rowSelected = -1;
    }
    editTask->setEnabled(false);
    deleteTask->setEnabled(false);
}

void Dialog::textChanged(QString dat)
{
    qDebug()<<dat;
    saveButton->setEnabled(true);
    saveAndExitButton->setEnabled(true);
}
void Dialog::descChanged()
{
    textChanged("ok");
}

void Dialog::dateTimeChanged(QDateTime dat)
{
    qDebug()<<dat;
    saveButton->setEnabled(true);
    saveAndExitButton->setEnabled(true);
}

//void Dialog::createMainLayout()


Dialog::Dialog(QWidget *parent)
    : QDialog(parent)
{
    /*
 * Main layout content
 */
    QStringList tablePan;
    rowSelected = -1;
    taskUniqID = 0;
    my_mode = SAMAYA_MODE_MAIN;
    mainLayout = new QGridLayout(this);
    mainLayoutButtonPanel = new QHBoxLayout();
    addTask = new QPushButton("+");
    deleteTask = new QPushButton("-");
    editTask = new QPushButton("edit");
    deleteTask->setEnabled(false);
    editTask->setEnabled(false);
    taskList= new QTableWidget(0, SAM_TABLE_ITEM_END);
    taskList->hideColumn(SAM_Act_End_Date);
    taskList->hideColumn(SAM_TaskUid);
    designerName = new QLabel("@designed by harinath");
    mainLayoutButtonPanel->addWidget(addTask, 0, Qt::AlignRight);
    mainLayoutButtonPanel->addWidget(deleteTask, 0, Qt::AlignRight);
    mainLayoutButtonPanel->addWidget(editTask, 0, Qt::AlignRight);
    mainLayout->addLayout(mainLayoutButtonPanel, 0, 0, 1, 1, Qt::AlignRight);
    //mainLayout->addWidget(deleteTask, 0, 2, 1, 1);
    mainLayout->addWidget(taskList, 1, 0, 1, 1);
    mainLayout->addWidget(designerName, 2, 0, 1, 1, Qt::AlignRight);
    taskList->setFixedWidth(WIDTH-40);
    taskList->setColumnWidth(0, taskList->width()*0.38);
    tablePan.append(SAMAY_TASK_NAME);
    tablePan.append(SAMAY_TASK_PRIORITY);
    tablePan.append(SAMAY_TASK_STATUS);
    tablePan.append(SAMAY_TASK_START_DATE);
    tablePan.append(SAMAY_TASK_END_DATE);
    tablePan.append(SAMAY_TASK_ACT_END_DATE);
    tablePan.append(SAMAY_TASK_UID);
    taskList->setHorizontalHeaderLabels(QStringList(tablePan));
    taskList->setColumnWidth(1, taskList->width()*0.126);
    taskList->setColumnWidth(2, taskList->width()*0.126);
    taskList->setColumnWidth(3, taskList->width()*0.171);
    taskList->setColumnWidth(4, taskList->width()*0.171);

    /******************************************************************/
    /*
 * Input layout content
 */
    inputLayout = new QGridLayout();
    for(int i =0; i < sizeof(inputLayoutBox)/sizeof(inputLayoutBox[0]); i++) {
        inputLayoutBox[i] = new QHBoxLayout();
        inputLayout->addLayout(inputLayoutBox[i], i, 0, 1, 1);
    }
    taskNameLabel = new QLabel(SAMAY_TASK_NAME" :");
    taskNameInput = new QLineEdit("add work name here");
    priorityLabel = new QLabel(SAMAY_TASK_PRIORITY" :");
    priorityInput = new QComboBox();
    priorityInput->addItem("low");
    priorityInput->addItem("medium");
    priorityInput->addItem("high");
    priorityInput->addItem("very high");
    statusLabel = new QLabel(SAMAY_TASK_STATUS" :");
    statusInput = new QComboBox();
    statusInput->addItem("not started");
    statusInput->addItem("started");
    statusInput->addItem("hold");
    statusInput->addItem("completed");
    taskStartTimeLabel = new QLabel(SAMAY_TASK_START_DATE" :");
    taskStartTime = new QDateTimeEdit();
    taskStartTime->setCalendarPopup(true);
    taskStartTime->setDisplayFormat(SAMAY_DATE_FORMAT);
    taskStartTime->setDateTime(QDateTime::currentDateTime());
    taskEndTimeLabel = new QLabel(SAMAY_TASK_END_DATE" :");
    taskEndTime = new QDateTimeEdit();
    taskEndTime->setCalendarPopup(true);
    taskEndTime->setDisplayFormat(SAMAY_DATE_FORMAT);
    taskEndTime->setDateTime(QDateTime::currentDateTime());
    taskActualEndTimeLabel = new QLabel(SAMAY_TASK_ACT_END_DATE" :");
    taskActualEndTime = new QDateTimeEdit();
    taskActualEndTime->setCalendarPopup(true);
    taskActualEndTime->setDisplayFormat(SAMAY_DATE_FORMAT);
    taskActualEndTime->setDateTime(QDateTime::currentDateTime());
    taskDescription = new QTextEdit("-");
    saveButton = new QPushButton("Save");
    saveAndExitButton = new QPushButton("Save and Exit");
    editButton = new QPushButton("Edit");
    exitButton = new QPushButton("Exit");

    //inputLayout->addLayout(inputLayoutBox[0], 0, 0, 1, 1);
    mainLayout->addLayout(inputLayout, 0, 0, 0, 0);
    inputLayoutBox[0]->addWidget(taskNameLabel);
    inputLayoutBox[0]->addWidget(taskNameInput);
    inputLayoutBox[1]->addWidget(priorityLabel);
    inputLayoutBox[1]->addWidget(priorityInput);
    inputLayoutBox[2]->addWidget(statusLabel);
    inputLayoutBox[2]->addWidget(statusInput);
    inputLayoutBox[0]->addWidget(taskStartTimeLabel);
    inputLayoutBox[0]->addWidget(taskStartTime);
    inputLayoutBox[0]->addWidget(saveButton);
    inputLayoutBox[0]->addWidget(exitButton);
    inputLayoutBox[1]->addWidget(taskEndTimeLabel);
    inputLayoutBox[1]->addWidget(taskEndTime);
    inputLayoutBox[1]->addWidget(saveAndExitButton);
    inputLayoutBox[2]->addWidget(taskActualEndTimeLabel);
    inputLayoutBox[2]->addWidget(taskActualEndTime);
    inputLayoutBox[2]->addWidget(editButton);

    inputLayout->addWidget(taskDescription);
    connect(addTask, SIGNAL(clicked(bool)), this, SLOT(addTaskSignal()));
    connect(deleteTask, SIGNAL(clicked(bool)), this, SLOT(deleteTaskSignal()));
    connect(exitButton, SIGNAL(clicked(bool)), this, SLOT(exitTaskSignal()));
    connect(saveButton, SIGNAL(clicked(bool)), this, SLOT(saveTaskSignal()));
    connect(saveAndExitButton, SIGNAL(clicked(bool)), this, SLOT(saveAndExitSignal()));
    connect(editButton, SIGNAL(clicked(bool)), this, SLOT(inputEditTaskSignal()));
    connect(editTask, SIGNAL(clicked(bool)), this, SLOT(editTaskSignal()));
    connect(taskList,SIGNAL(cellPressed(int,int)), this, SLOT(tableCellSelected(int,int)));
    connect(taskList,SIGNAL(cellDoubleClicked(int,int)), this, SLOT(tableCellDoubleClicked(int,int)));
    connect(taskNameInput, SIGNAL(textChanged(QString)), this, SLOT(textChanged(QString)));
    connect(taskDescription, SIGNAL(textChanged()), this, SLOT(descChanged()));
    connect(priorityInput , SIGNAL(currentTextChanged(QString)), this, SLOT(textChanged(QString)));
    connect(statusInput, SIGNAL(currentTextChanged(QString)), this, SLOT(textChanged(QString)));
    connect(taskStartTime, SIGNAL(dateTimeChanged(QDateTime)), this, SLOT(dateTimeChanged(QDateTime)));
    connect(taskEndTime, SIGNAL(dateTimeChanged(QDateTime)), this, SLOT(dateTimeChanged(QDateTime)));
    connect(taskActualEndTime, SIGNAL(dateTimeChanged(QDateTime)), this, SLOT(dateTimeChanged(QDateTime)));
    accessMainLayout(true);
    accessInputLayout(false);
    updateTable();
    windowPositioning();
}

Dialog::~Dialog()
{

}
